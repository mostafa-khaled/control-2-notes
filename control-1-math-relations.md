## Time domain specifications
$$Y(S) = \frac{\omega^2}{s^2 + 2\zeta\omega s + \omega^2} R(S)$$

$$Y(S) = \frac{G_cG}{1 + G_cG} \quad \text{for unitiy feedback}$$

$$E(S) = Y(S) - R(S)$$

$$U(S) = E(S) \cdot G_c(S)$$

$$\boxed{y(t) = 1 - \frac{e^{-\zeta \omega t}}{\sqrt{1 -
\zeta^2}}\sin(\omega_d t + \theta),\quad\theta = \cos^{-1}(\zeta)}$$

$$\omega_d = \omega \sqrt{1 - \zeta^2}$$

$$\sigma = \zeta\omega$$

$$M_p = e^{\frac{-\zeta\pi}{\sqrt{1 - \zeta^2}}} = e^{-\frac{\sigma\pi}{\omega_d}}$$

$$\zeta = \sqrt{\frac{\log^2{M_p}}{\log^2{M_p}+\pi^2}}$$
- **note**: $\log$ here is $\log_e$

$$\boxed{POS = 100M_p}$$

$$\boxed{t_p = \frac{\pi}{\omega_d}}$$

$$\boxed{t_{r(0-1)} = \frac{\pi - \theta}{\omega_d}}$$

$$\boxed{t_{r(.1-.9)} \approx \frac{1.8}{\omega}}$$

$$\boxed{t_{s(2\%)} \approx \frac{4}{\zeta \omega}}$$

$$\boxed{t_{s(5\%)} \approx \frac{3}{\zeta \omega}}$$

$$\boxed{e_{ss(step)} = \frac{1}{1 + K_p},\quad K_p = \lim_{s \rightarrow 0} \frac{\omega^2}{s (s + 2\zeta\omega)}}$$

$$\boxed{e_{ss(ramp)} = \frac{1}{K_v},\quad K_v = \lim_{s \rightarrow 0} \frac{\omega^2}{s + 2\zeta\omega}}$$

$$\boxed{e_{ss(parapola)} = \frac{1}{K_a},\quad K_a = \lim_{s \rightarrow 0} s \frac{\omega^2}{s + 2 \zeta \omega}}$$

$$\boxed{s_d,\bar{s_d} = -\zeta \omega \pm j\omega_d}$$

## Root locus

$$\text{\#asyptotes} = |P| - |Z|$$

$$\theta_{n} = \frac{180(2n + 1)}{|P|-2}$$

$$\text{Centroid} = \frac{\Sigma{P} - \Sigma{Z}}{|P|-|Z|}$$

$$\text{break-away/in} = \text{solutions of }\left\{\frac{dK}{ds} = 0\right\}$$

$$\text{cross-over} = \text{solutions of auxiliary equation}$$



## Frequency domain specifications

*NOTE*: It is easy to derive this equation.

$$\boxed{M(j\omega) = \frac{1}{1 - \left(\frac{\omega}{\omega}\right)^2 + j 2 \zeta \frac{\omega}{\omega}}}$$

$$|M|_{dB} = 20\log\left(\frac{\frac{\omega}{2\zeta}}{\omega\sqrt{1 +
    \frac{1}{4\zeta^2}\left(\frac{\omega}{\omega}\right)^2}}\right)$$

$$\angle{M} = -tan^{-1}
\left(
  \frac{2\zeta
    \left(
      \frac{\omega}
           {\omega}
    \right)}
    { 1 - \left(
          \frac{\omega}
	       {\omega}
	  \right)^2}
\right)$$

$$\omega_{pc} = \angle{G|_{|M| = GM}}$$

$$\boxed{\omega_r = \omega\sqrt{1-2\zeta^2},\quad \text{if } \zeta \leq
\frac{1}{\sqrt{2}}}$$

$$\boxed{M_r = |M|_{\omega=\omega_r} = \frac{1}{2\zeta\sqrt{1-\zeta^2}},\quad \text{if }
\zeta \leq \frac{1}{\sqrt{2}}}$$

$$\omega_b = \omega \sqrt{1-2\zeta^2+\sqrt{2-4\zeta^2+4\zeta^4}}$$

$$\boxed{PM = \tan^{-1}\left( \frac{2\zeta}{\sqrt{-2\zeta^2 + \sqrt{1 + 4 \zeta^4}}}
    \right) = \tan^{-1}\left(\frac{2\zeta\omega}{\omega_{gc}}\right)}$$

$$\boxed{\omega_{gc} = \omega \sqrt{- 2\zeta^2 + \sqrt{1 + 4\zeta^4}} }$$

$$\angle{G|_{\omega = \omega_{gc}}} = -90 - \tan^{-1}\left(\frac{\sqrt{-2\zeta^2
    + \sqrt{4\zeta^4 + 1}}}{2\zeta}\right)$$

## Some old math relations

### Final value theorem

$$f(\infty) = \lim_{s \rightarrow 0}{sF(s)}$$

### Trigonometry

$$\sin(x_1 \pm x_2) = \sin(x_1)\cos(x_2) \pm \cos(x_1)\sin(x_2)$$

$$\cos(x_1 \pm x_2) = \cos(x_1)\cos(x_2) \mp \sin(x_1)\sin(x_2)$$

$$\frac{A}{\sin(a)} = \frac{B}{\sin(b)} = \frac{C}{\sin(c)}$$
whee a is the angle next to A and so on
